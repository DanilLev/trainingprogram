using Serilog;
using TrainingProgram.Infrastructure.PostgresIdentity;

namespace TrainingProgram.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {

            try
            {
                Log.Information("������ ������� TrainingIdentity");
                var host = CreateHostBuilder(args).Build();
                using (var scope = host.Services.CreateScope())
                {
                    var db = scope.ServiceProvider.GetRequiredService<DbContextPostgress>();
                }
                Log.Information("TrainingIdentityService ������ ����������");
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "������ ��� ������� ����������");
            }
            finally
            {
                Log.Information("TrainingIdentity ������ ��������� ���� ������");
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {

                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
                    {
                    });
                });

    }
}