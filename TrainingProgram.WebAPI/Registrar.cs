﻿using Serilog;
using Trainingprogram.RepositoriesAbstractions.UserRepository;
using Trainingprogram.Services.Abstractions.Admin;
using Trainingprogram.Services.Abstractions.Token;
using Trainingprogram.Services.Abstractions.User;
using TrainingProgram.Entities.Settings;
using TrainingProgram.Infrastructure.PostgresIdentity;
using TrainingProgram.Infrastructure.PostgresIdentity.Infrastructure.Repositories.Implementations.UserManager;
using TrainingProgram.services.Administration;
using TrainingProgram.Services.OAuth;
using TrainingProgram.WebAPI.Models;

namespace TrainingProgram.WebAPI
{
    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
                .WriteTo.Seq("http://localhost:5341/",
                apiKey: "uEysVuHNPVVtRgkkP23N")
                .CreateLogger();

            services.AddSingleton((IConfigurationRoot)configuration)
                    .InstallServices()
                    .ConfigureContextPostgres(configuration)
                    .Configure<JwtSettings>(configuration.GetSection(JwtSettings.DefaultSection))
                    .InstallRepositories();

            return services;
        }
        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
            .AddHttpContextAccessor()
            .AddTransient<ICourseCommandPublisher, CourseCommandPublisher>()
            .AddTransient<ITokenService, TokenService>()
            .AddTransient<IUserService, UserServices>()
            .AddTransient<IAdminService, AdminService>()
            .AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            

            return serviceCollection;
        }
        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<ITokenRepository, TokenRepository>()
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<IRoleRepository, RoleRepository>()
                .AddTransient<IRolesUserRepository, RoleUsersRepository>();
            return serviceCollection;
        }

    }
}
